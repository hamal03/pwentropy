#!/usr/bin/perl

# This script is used for estimating password strengths given a specific
# password generating algorithm.
# See https://techblog.hamal.nl/passwords.html for a description of the
# algorithm.

use strict;
use warnings;
use Getopt::Long;

Getopt::Long::Configure('gnu_getopt');

my ($HELP,$WORDS,$NOUNS,$NAMES,$MAP,$SKIPNUM,$QUIET)=();

GetOptions(
    'help|h' => \$HELP,
    'quiet|q' => \$QUIET,
    'words|w=s' => \$WORDS,
    'nouns|n=s' => \$NOUNS,
    'names|propernames|p=s' => \$NAMES,
    'map|m=s' => \$MAP,
    'nodigits' => \$SKIPNUM,
);
&Usage if (defined $HELP);

die "--words option is mandatory\n" if (not defined $WORDS);
# List of known words
my %dict=();
open( my $f, "<", $WORDS ) or die "Cannot open words file\n";
while (<$f>) {
    chomp;
    $dict{lc($_)} = 1;
}
close $f;

# List of names (fist names and surnames)
my %name=();
if (defined $NAMES) {
    open( $f, "<", $NAMES ) or die "cannot open names file\n";
    while (<$f>) {
        chomp;
        $name{lc($_)} = 1;
    }
    close $f;
}

# List of nouns
my %noun=();
if (defined $NOUNS) {
    open( $f, "<", $NOUNS ) or die "cannot open nouns file\n";
    while (<$f>) {
        chomp;
        $noun{lc($_)} = 1;
    }
    close $f;
}

my %mapped = ();
if (defined $MAP) {
    open( $f, "<", $MAP ) or die "cannot open nouns file\n";
    while (<$f>) {
        chomp;
        next if (/^\s*#/); #comment
        my @maparr=split('\t');
        next if (scalar(@maparr) != 2);
        $mapped{$maparr[0]} = $maparr[1];
    }
    close $f;
}

my %total = ();
my $unknown = 0;
my $added = 0;

my $counter = 0;
for my $file ( @ARGV ) {
    $counter = 0;
    open( my $f, "<", $file ) or die "Cannot open $file\n";
    while (<$f>) {
        chomp;
        my @words = split( ' ', lc($_) );
        for my $word (@words) {
            # cleanup: remove leading and trailing quotes and
            # braces and trailing punctuation
            $word =~ s/^["'\(]?(.*?)["'!?\.,\):;]$/$1/;
            # fix pronouns
            $word =~ s/^(i|you|we|they|s?he)'(re,ve,m,d,s)$/$1/;
            if ( exists $mapped{$word} ) {
                if ( $mapped{$word} =~ /^\d+$/ ) {
                    AddDigits( $mapped{$word} );
                }
                else {
                    AddCount( $mapped{$word} );
                }
            }
            elsif ( $word =~ /^\d+$/ ) {
                AddDigits( $word );
            }
            elsif ( IsNounOrName($word) ) {
                AddCount( uc($word) );
            }
            elsif ( exists $dict{$word} ) {
                AddCount( lc($word) );
            }
            else {
                $unknown++;
            }
        }
    }
    close $f;
}
print STDERR "\n" if (not defined $QUIET);

print "# Total tabulated: ${added}\n";
print "# Total skipped: ${unknown}\n\n";
print "# Format: char<TAB>count<TAB>frequency<TAB>entropy\n\n";
for my $char ( sort { $total{$b} <=> $total{$a} } keys(%total) ) {
    my $freq=$total{$char}/$added;
    my $entropy = log(1/$freq)/log(2);
    printf ( "%s\t%d\t%03f\t%03f\n", $char, $total{$char}, $freq, $entropy );
}

sub AddCount {
    if (not defined $QUIET) {
        if ( ! ($counter++ % 1000) ) {
            print STDERR ".";
        }
    }
    $added++;
    my ($word) = @_;
    my ($char) =  split( //, $word );
    $total{$char} = 0 if ( not exists $total{$char} );
    $total{$char}++;
}

sub AddDigits {
    # if --nodigits is set, don't count them
    return if (defined $SKIPNUM);
    # Numbers are entered integral into the password
    # so all of them count
    my ($number) = @_;
    my @digits = split( //, $number );
    for my $digit (@digits) {
        AddCount($digit);
    }
}

sub IsNounOrName {
    my ($arg) = @_;
    return 1 if ( exists $noun{$arg} );
    return 1 if ( exists $name{$arg} );
    # try singular noun
    $arg =~ s/([os])es$/$1/; # potatoes, buses
    $arg =~ s/ies$/y/; # babies
    return 1 if ( exists $noun{$arg} );
    return 0;
}

sub Usage {
    print STDERR "\nUsage: $0 --help\n";
    print STDERR "       $0 -w | --words /path/to/wordlist [ -n | --nouns /path/to.nounslist ]\n";
    print STDERR "          [ -p | --propernames | --names /path/to/nameslist ]\n";
    print STDERR "          [ -m | --map /path/to/symbolmap ] [ -q | --quiet ] [ --nodigits ]\n\n";
    exit 0;
}