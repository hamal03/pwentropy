# Password strength estimation

These files are referenced in a blog post at
https://techblog.hamal.nl/passwords.html which deals with a method to generate
strong passwords that are easy to remember. The scripts and files in this
repository deal with estimating the strength of passwords using the described
method.

The primary script (startchar.pl) uses input texts to calculate the frequency of
leading characters in English words. Given the method, nouns and names should be
capitalized and some word to symbol mapping is done. The script uses a file with
English words (mandatory), a file with nouns, a file with proper names and a file
with word to symbol mapping (all optional) and prints a tab separated text to
STDOUT with character frequencies and entropy in bits for each character.

A second script (entropy.pl) uses the above mentioned output to calculate
the entropy in bits of a given password that is presumably generated with the
method in the blog page.
