#!/usr/bin/perl

use strict;
use warnings;

my $freqfile;
if (scalar(@ARGV) > 2) {
    if ($ARGV[0] eq '-f') {
        $freqfile=$ARGV[1];
        splice(@ARGV,0,2);
    }
}
$freqfile = "./frequencies.tsv" if (not defined $freqfile);

my ($pw) = @ARGV;
die "Password in argument\n" if (not $pw);

open(my $F, "<", $freqfile) or die "Cannot open $freqfile for frequencies\n";
my %charent=();
# frequency file format:
# character<TAB>count<TAB>frequency<TAB>entropy
while (<$F>) {
    chomp;
    my @line=split('\t');
    next if (scalar(@line) != 4);
    $charent{$line[0]}=$line[3];
}
close($F);
die "Invalid frequencies file\n" if (not scalar(keys(%charent)));

my $bpc = log(scalar(keys(%charent)))/log(2);
my $ent = 0;
for my $pwchar ( split( //, $pw ) ) {
    die "Unknown char: $pwchar\n" if ( not exists $charent{$pwchar} );
    $ent += $charent{$pwchar};
}
printf("Password:\t%s\n",$pw);
printf("RandomEntropy:\t%.3f\n",$bpc*length($pw));
printf("PW entropy:\t%.3f\n",$ent);
